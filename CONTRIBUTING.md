# Contributing

*Needs more work*

1. Fork into your own repository
2. Make changes locally
3. Run `clang-format`
4. Test changes locally
5. Submit pull request
6. Wait for pipeline and approval
7. Finally accepted
