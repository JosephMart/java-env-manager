#include <experimental/filesystem>
#include <string>
#include <vector>

#include "doctor.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

bool check_duplicate_names() noexcept
{
    return true;
}

bool check_duplicate_locations() noexcept
{
    return true;
}

bool check_duplicate_sets() noexcept
{
    return true;
}

bool check_json_keys() noexcept
{
    return true;
}

bool program_folder_exists() noexcept
{
    return true;
}

void cmd::doctor(std::vector<std::string> args, bool debug) noexcept
{
    return;
}
