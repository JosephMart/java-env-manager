#ifndef _SET_HH_
#define _SET_HH_

#include <string>
#include <vector>

namespace cmd
{
    void set(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _SET_HH_
