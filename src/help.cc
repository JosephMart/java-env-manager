#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "help.hh"
void cmd::help(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() <= 1) {
        std::cout << "Not enough arguments\n";
        help::help();
    } else if (args.size() == 2) {
        // mapping each argument to their print functions
        const std::map<std::string, std::function<void()>> extra_help = {
            {"add", help::add},     {"help", help::help},     {"init", help::init},
            {"list", help::list},   {"remove", help::remove}, {"set", help::set},
            {"which", help::which}, {"update", help::update}, {"version", help::version}};

        // find and run print function
        auto temp = extra_help.find(args[1]);
        if (temp != extra_help.end()) {
            temp->second();
        } else {
            std::cout << args[1] << " not a valid subcommand\n";
            help::help();
            exit(1);
        }
    } else {
        std::cout << "Too many arguments.\n";
    }
}

void help::add() noexcept
{
    std::cout << "usage: java-env-manager add [name] [path]\n"
              << "\tname is what you want to reference the path by\n"
              << "\tpath is the location of the Java distribution you want to add\n";
}

void help::doctor() noexcept {}

void help::help() noexcept
{
    std::cout << "usage: java-env-manager [options] [commands] [arguments]\n\n"
              << "Commands include:\n"
              << "\tadd\tAdd a Java distribution\n"
              << "\tdoctor\tPrint errors with the settings.json file, and make suggestions to "
                 "fix them\n"
              << "\thelp\tPrint the help command\n"
              << "\tinit\tInitialize the Java Environment Manager\n"
              << "\tlist\tPrint all possible Java distributions\n"
              << "\tremove\tRemove a Java distribution\n"
              << "\tset\tSet the current Java distribution\n"
              << "\tupdate\tUpdate the distribution a name points to\n"
              << "\tversion\tPrint the current Java distribution\n"
              << "\twhich\tPrint the path to the current Java executables\n"
              << "\nOptions include:\n"
              << "\t-d, --debug\tAdd debug logging to commands\n"
              << "\t-v, --version\tPrint the version of the Java Environment Manager\n";
}

void help::init() noexcept {}

void help::list() noexcept {}

void help::remove() noexcept {}

void help::set() noexcept {}

void help::update() noexcept {}

void help::version() noexcept {}

void help::which() noexcept {}