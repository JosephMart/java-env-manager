#include <experimental/filesystem>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "add.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

void cmd::add(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() != 3) {
        std::cout << "Not enough arguments\n";
        exit(1);
    }

    if (!fs::is_directory(args[2]) && !fs::is_symlink(args[2])) {    // TODO testsymlink
        std::cout << args[2] << " is not a directory\n";
        exit(1);
    }

    json j;
    try {
        j = util::get_settings();
    } catch (std::runtime_error e) {
        std::cout << e.what() << '\n' << "Run 'java-env-manager init' to get started.\n";
        exit(1);
    }

    for (const json &distro : j["distributions"]) {
        if (distro["name"].get<std::string>().compare(args[1]) == 0) {
            std::cout << args[1] << " already exists (" << distro["location"].get<std::string>()
                      << ")\n";
            exit(1);
        }
    }

    json distro = {{"name", args[1]}, {"location", args[2]}, {"set", false}};
    j["distributions"].push_back(distro);

    util::set_settings(j);
}
