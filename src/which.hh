#ifndef _WHICH_HH_
#define _WHICH_HH_

#include <string>
#include <vector>

namespace cmd
{
    void which(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _WHICH_HH_
