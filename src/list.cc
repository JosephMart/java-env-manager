#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include "list.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

void cmd::list(std::vector<std::string> args, bool debug) noexcept
{
    json j;
    try {
        j = util::get_settings();
    } catch (std::runtime_error e) {
        std::cout << e.what() << '\n' << "Run 'java-env-manager init' to get started.\n";
        exit(1);
    }

    std::string prepend;
    for (const json &distro : j["distributions"]) {
        if (distro["set"].get<bool>()) {
            prepend = "* ";
        } else {
            prepend = "  ";
        }
        std::cout << prepend << distro["name"].get<std::string>() << '\n';
    }
}
