#ifndef _UPDATE_HH_
#define _UPDATE_HH_

#include <string>
#include <vector>

namespace cmd
{
    void update(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _UPDATE_HH_
