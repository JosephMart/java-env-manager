#include <experimental/filesystem>
#include <iostream>
#include <string>
#include <vector>

#include "nlohmann/json.hpp"
#include "set.hh"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

void cmd::set(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() != 2) {
        std::cout << "Only 2 arguments accepted\n";
        exit(1);
    }

    std::string program_dir     = util::get_program_dir();
    std::string program_bin_dir = util::get_program_bin_dir(program_dir);
    json j;
    try {
        j = util::get_settings(program_dir);
    } catch (std::runtime_error e) {
        std::cout << e.what() << '\n' << "Run 'java-env-manager init' to get started.\n";
        exit(1);
    }

    for (json &distro : j["distributions"]) {
        if (distro["name"].get<std::string>().compare(args[1]) == 0) {
            distro["set"] = true;

            std::string target = distro["location"].get<std::string>();
            target = target.find_last_of('/') == target.size() - 1 ? target : target + '/';

            fs::remove(program_bin_dir);
            fs::create_symlink(target, program_bin_dir);
            util::set_settings(j, program_dir);
            break;
        }
    }
}
