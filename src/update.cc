#include <experimental/filesystem>
#include <iostream>
#include <string>
#include <vector>

#include "nlohmann/json.hpp"
#include "update.hh"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

void cmd::update(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() != 3) {
        std::cout << "Not enough arguments\n";
        exit(1);
    }

    if (!fs::is_directory(args[2]) && !fs::is_symlink(args[2])) {    // TODO testsymlink
        std::cout << args[2] << " is not a directory\n";
        exit(1);
    }

    json j;
    try {
        j = util::get_settings();
    } catch (std::runtime_error e) {
        std::cout << e.what() << '\n' << "Run 'java-env-manager init' to get started.\n";
        exit(1);
    }

    for (json &distro : j["distributions"]) {
        if (distro["name"].get<std::string>().compare(args[1]) == 0) {
            distro["location"] = args[2];
        }
    }

    util::set_settings(j);
}
