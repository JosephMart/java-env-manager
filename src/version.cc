#include <iostream>
#include <string>
#include <vector>

#include "nlohmann/json.hpp"
#include "utility.hh"
#include "version.hh"

using json = nlohmann::json;

void cmd::version(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() != 1) {
        std::cout << "Too many arguments\n";
        exit(1);
    }

    json j;
    try {
        j = util::get_settings();
    } catch (std::runtime_error e) {
        std::cout << e.what() << '\n' << "Run 'java-env-manager init' to get started.\n";
        exit(1);
    }

    bool found = false;
    for (const json &distro : j["distributions"]) {
        if (distro["set"].get<bool>()) {
            found = true;
            std::cout << distro["name"] << '\n';
        }
    }

    if (!found) {
        std::cout << "No current Java version found. Use \"java-env-manager set\" to set one\n";
        exit(1);
    }
}
