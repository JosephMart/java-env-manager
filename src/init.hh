#ifndef _INIT_HH_
#define _INIT_HH_

#include <string>
#include <vector>

namespace cmd
{
    void init(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _INIT_HH_
