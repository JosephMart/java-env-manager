#ifndef _DOCTOR_HH_
#define _DOCTOR_HH_

#include <string>
#include <vector>

bool check_duplicate_names() noexcept;
bool check_duplicate_locations() noexcept;
bool check_duplicate_sets() noexcept;
bool check_json_keys() noexcept;
bool program_folder_exists() noexcept;

namespace cmd
{
    void doctor(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _DOCTOR_HH_
