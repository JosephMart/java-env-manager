#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "utility.hh"
#include "which.hh"

namespace fs = std::experimental::filesystem;

void cmd::which(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() > 1) {
        std::cout << "Too many arguments." << '\n';
        exit(1);
    }

    std::string program_bin_path = util::get_program_bin_dir();
    if (!fs::exists(program_bin_path)) {
        std::cout << "bin directory does not exist." << '\n';
        exit(1);
    }

    std::cout << fs::read_symlink(program_bin_path) << '\n';
}
