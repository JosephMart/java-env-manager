#include <string>

#include "nlohmann/json.hpp"

#define FOLDER_NAME "/.java-env-manager/"    // default folder name
#define LOG_FILE "log.out"                   // name of log file
#define SETTINGS_FILE "settings.json"        // name of settings file
#define VERSION "0.0.1"                      // version of Java Environment Manager

using json = nlohmann::json;

namespace util
{
    /*
     * Returns the Java Environment Manager home directory
     */
    const std::string get_program_dir() noexcept;

    /*
     * Returns the Java Environment Manager bin directory
     */
    const std::string get_program_bin_dir(std::string program_dir = std::string()) noexcept;

    /*
     * Returns the JSON representation of the settings file
     */
    json get_settings(std::string program_dir = std::string());

    /*
     * Sets the settings using a JSON object
     */
    void set_settings(json &j, std::string program_dir = std::string()) noexcept;
}
