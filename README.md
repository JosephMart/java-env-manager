# Java Environment Manager

This program is meant to allow ease of use when switching between various Java
distributions.

## How it Works

A Java distribution comes packaged like this:

```text
jdk/
├── bin
│   └── ...
├── conf
│   └── ...
├── include
│   └── ...
├── jmods
│   └── ...
├── legal
│   └── ...
├── lib
│   └── ...
├── man
│   └── ...
└── release
```

When you install the distrbution through your package manager, your executables
can get overwritten if you have both JDK 7 and JDK 8 installed. Java Environment
Manager will create symlinks to the binaries which should be located in something
akin to `/usr/lib64/openjdk-8/bin` into `JAVA_ENV_MANAGER_HOME/bin`. From there
the `JAVA_ENV_MANAGER_HOME/bin` will need to be added to your `PATH` before
`/usr/bin`. Now it is off to the races. You can change distributions
dynamically on a project-by-project basis.

## How to Build

### Dependencies

* `meson` (Project is currently built using `>= 0.42.1`. Not sure of the minimum
version it can be compiled with)
* `C++ >= C++17` (Liberal usage of `std::filesystem`)
    * Willing to accept pull requests to backport to older C++ standards no
        older than C++11
* `clang++` or `g++` (Not sure of minimum versions. I use `g++ >= 7 / clang++ >= 5`)
* Third Party
        * https://github.com/nlohmann/json (`json.hpp` located in tree)

### Commands

```text
CXX=[C++ compiler] meson build
cd build/
ninja
```

Executable is located at `src/java-env-manager`.

```text
usage: java-env-manager [options] [commands] [arguments]

Commands include:
        add     Add a Java distribution
        doctor  Print errors with the settings.json file, and make suggestions to fix them
        help    Print the help command
        init    Initialize the Java Environment Manager
        list    Print all possible Java distributions
        remove  Remove a Java distribution
        set     Set the current Java distribution
        update  Update the distribution a name points to
        version Print the current Java distribution
        which   Print the path to the current Java executables

Options include:
        -d, --debug     Add debug logging to commands
        -v, --version   Print the version of the Java Environment Manager
```

## Testing

### Running Tests

```text
cd build/
ninja test
```

### Developing Tests

Each command of `java-env-manager` will have its own set of tests located within `tests/[command].cc`.

For instance, here is a possible `init.cc` test file:

```c++
#include <iostream>

bool check_folder_creation()
{
    // testing code goes here
    // if passed return true, else return false
}

int main(const int argc, char *const *argv)
{
    if (!check_folder_creation()) {
        return 1;
    }

    return 0;
}
```

Adding to `tests/` Meson file:

```text
add = executable(
    'init',
    sources: ['init.cc', '../src/init.cc', '../src/utility.cc'],
    include_directories: include_dirs
)
test('Init Tests', init)
```

Find more information [here](http://mesonbuild.com/Unit-tests.html).

## Roadmap to 1.0

1. Creating the `doctor` command
2. Complete unit testing
3. Set up logging

## Contributors

* @acoronado1234
